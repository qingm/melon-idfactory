package com.fetech.melon.idfacotry.client;

import com.fetech.melon.idfacotry.client.api.IdFactoryApi;
import com.fetech.melon.rpc.consumer.RpcContext;
import com.fetech.melon.rpc.support.entity.RpcResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ZhangGang on 2017/10/24.
 */
@Service
public class IdFactoryClient implements IdFactoryApi {

    @Resource
    private RpcContext rpcContext;

    private static final String ServerName = "IdFactory";

    @Override
    public long getIncrementId(String key) {
        RpcResult id = rpcContext.call(ServerName, "getIncrementId", new Object[]{key});
        return (long) id.getData();
    }

    @Override
    public boolean initIncrementId(String key, long start) {
        RpcResult rpcResult = rpcContext.call(ServerName, "initIncrementId", new Object[]{key, start});
        return rpcResult.isSuccess();
    }

    @Override
    public String getUUID() {
        RpcResult id = rpcContext.call(ServerName, "getUUID", null);
        return (String) id.getData();
    }

    @Override
    public long get64Uid() {
        RpcResult id = rpcContext.call(ServerName, "get64Uid", null);
        return (long) id.getData();
    }

    @Override
    public String parse64Uid(long uid) {
        RpcResult rpcResult = rpcContext.call(ServerName, "parse64Uid", new Object[]{uid});
        return (String) rpcResult.getData();
    }
}
