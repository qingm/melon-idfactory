package com.fetech.melon.idfactory.rpc.impl;

import com.fetech.melon.context.utils.MelonUtils;
import com.fetech.melon.idfactory.impl.CachedUidGenerator;
import com.fetech.melon.idfactory.rpc.IdFactory;
import com.fetech.melon.idfactory.increment.IncrementLongService;
import com.fetech.melon.rpc.provider.annotation.RpcCaller;
import com.fetech.melon.rpc.provider.annotation.RpcRestful;

import javax.annotation.Resource;

/**
 * Created by ZhangGang on 2017/9/29.
 */
@RpcRestful
@RpcCaller("IdFactory")
public class IdFactoryImpl implements IdFactory {

    @Resource
    private CachedUidGenerator cachedUidGenerator;
    @Resource
    private IncrementLongService incrementLong;

    @RpcCaller
    public long getIncrementId(String key) {
        return incrementLong.getNewId(key);
    }

    @RpcCaller
    public void initIncrementId(String key, long start) {
        incrementLong.init(key, start);
    }

    @RpcCaller
    public String getUUID() {
        return MelonUtils.getUUID();
    }

    @RpcCaller
    public long get64Uid() {
        return cachedUidGenerator.getUID();
    }

    @RpcCaller
    public String parse64Uid(long uid) {
        return cachedUidGenerator.parseUID(uid);
    }
}
