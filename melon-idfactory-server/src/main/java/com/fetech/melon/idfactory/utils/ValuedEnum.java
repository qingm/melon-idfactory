package com.fetech.melon.idfactory.utils;

public interface ValuedEnum<T> {
    T value();
}
