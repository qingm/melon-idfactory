package com.fetech.melon.idfactory.increment;

import com.fetech.melon.dao.mongodb.BaseMongoEntity;

/**
 * Created by ZhangGang on 2017/10/20.
 */
public class IncrementLong extends BaseMongoEntity {

    private static final long serialVersionUID = -6951783976903050822L;

    private String key;
    private long curId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getCurId() {
        return curId;
    }

    public void setCurId(long curId) {
        this.curId = curId;
    }
}
